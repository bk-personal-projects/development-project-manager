$(function() {

	var FirebaseConfiguration = {

		// METHODS
		"Authentication": function(options) {
			var firebaseDB = options.database;
		
			if(options.action == "login") {
				// SUBMIT THE EMAIL AND PASSWORD
				firebaseDB.auth().signInWithEmailAndPassword(options.email + "@blackboard.com", options.password).then(function() {
					// RELOAD THE PAGE
					location.reload(true);
				}, function(errorObject) {
					// PROVIDE ERROR MESSAGE
					ProjectManager.LightBox({
						"message": '<h2>Uh Oh!</h2><div class="light-box-message">Something happened when trying to log you in. The error was:<br /><br/><span style="color: #990000;">"' + errorObject.code + ': ' + errorObject.message + '"</span></div>',
						"confirm": function() {
							$("#login-form .loader-working").hide();
						},
						"cancel": function() {},
						"showCancel": false
					});
				});
			} else if(options.action == "logout") {
				// LOG OUT
				firebaseDB.auth().signOut().then(function() {
					// RELOAD THE PAGE
					location.reload(true);
				}, function(errorObject) {
					// PROVIDE ERROR MESSAGE
					ProjectManager.LightBox({
						"message": '<h2>Uh Oh!</h2><div class="light-box-message">Something happened when trying to log you out. The error was:<br /><br/><span style="color: #990000;">"' + errorObject.code + ': ' + errorObject.message + '"</span></div>',
						"confirm": function() {},
						"cancel": function() {},
						"showCancel": false
					});
				});
			}
		},
		
		"IsAuth": function(reference) {
			return (reference.auth().currentUser) ? true : false;
		},

		"Config": function(options) {
			// FIREBASE CONFIGURATION
			var config = {
				apiKey: options.apiKey,
				authDomain: options.authDomain,
				databaseURL: options.databaseURL,
				storageBucket: options.storageBucket
			};

			// INITIALIZE THE FIREBASE
			var FirebaseApp = firebase.initializeApp(config);

			// RETURN THE FIREBASE INSTANCE
			return FirebaseApp;
		},

		"GetData": function(options) {
			var FirebaseDB = options.firebaseApp.database();

			FirebaseDB.ref(options.ref).on("value", function(snapshot) {
				// RUN CALLBACK FUNCTION
				options.ready(snapshot);
			}, function (errorObject) {
				// PROVIDE ERROR MESSAGE
				ProjectManager.LightBox({
					"message": '<h2>Uh Oh!</h2><div class="light-box-message">Something happened when trying to get the records from Firebase. The error was:<br /><br/><span style="color: #990000;">"' + errorObject.code + ': ' + errorObject.message + '"</span></div>',
					"confirm": function() {},
					"cancel": function() {},
					"showCancel": false
				});
				
				return false;
			});
		},

		"Action": function(options) {
			var FirebaseDB = options.firebaseApp.database();

			switch(options.action) {
				// INSERT NEW RECORD
				// FIREBASE - Add to a list of data. Every time you call push(), Firebase generates a unique key that can also be used as a unique identifier.
				case "insert":
					var newKey = FirebaseDB.ref(options.ref).push().key;
				
					FirebaseDB.ref(options.ref + newKey).update(options.recordData).then(function() {
						// SHOW NOTIFICATION
						if(options.successMsg != "") {
							ProjectManager.Notify(options.successMsg);
						}
						
						// CUSTOM CALLBACK
						var returnedData = {
							"newKey": newKey
						};
						
						if(options.callback != undefined) {
							options.callback(returnedData);
						}
					}, function(error) {
						ProjectManager.LightBox({
							"message": '<h2>Uh Oh!</h2><div class="light-box-message">Something happened when trying to add the record. Please try again and/or tell Brenton about this and give him the following error code:<br /><br/><span style="color: #990000;">"' + error.code + ': ' + error.message + '"</span></div>',
							"confirm": function() {},
							"cancel": function() {},
							"showCancel": false
						});
					});
				break;

				// UPDATE EXISTING RECORD
				// FIREBASE - Update some of the keys for a defined path without replacing all of the data.
				case "update":
					FirebaseDB.ref(options.ref + options.recordID).update(options.recordData).then(function() {
						// SHOW NOTIFICATION
						if(options.successMsg != "") {
							ProjectManager.Notify(options.successMsg);
						}
						
						// CUSTOM CALLBACK
						if(options.callback != undefined) {
							options.callback();
						}
					}, function(error) {
						ProjectManager.LightBox({
							"message": '<h2>Uh Oh!</h2><div class="light-box-message">Something happened when trying to update the record. Please try again and/or tell Brenton about this and give him the following error code:<br /><br/><span style="color: #990000;">"' + error.code + ': ' + error.message + '"</span></div>',
							"confirm": function() {},
							"cancel": function() {},
							"showCancel": false
						});
					});
				break;

				// REPLACE DATA FOR EXISTING RECORD
				// FIREBASE - Write or replace data to a defined path.
				case "replace":
					FirebaseDB.ref(options.ref + options.recordID).set(options.recordData).then(function() {
						// SHOW NOTIFICATION
						if(options.successMsg != "") {
							ProjectManager.Notify(options.successMsg);
						}
						
						// CUSTOM CALLBACK
						if(options.callback != undefined) {
							options.callback();
						}
					}, function(error) {
						ProjectManager.LightBox({
							"message": '<h2>Uh Oh!</h2><div class="light-box-message">Something happened when trying to update the record. Please try again and/or tell Brenton about this and give him the following error code:<br /><br/><span style="color: #990000;">"' + error.code + ': ' + error.message + '"</span></div>',
							"confirm": function() {},
							"cancel": function() {},
							"showCancel": false
						});
					});
				break;

				// DELETE RECORD
				case "delete":
					FirebaseDB.ref(options.ref + options.recordID).remove().then(function() {
						// SHOW NOTIFICATION
						if(options.successMsg != "") {
							ProjectManager.Notify(options.successMsg);
						}
						
						// CUSTOM CALLBACK
						if(options.callback != undefined) {
							options.callback();
						}
					}, function(error) {
						ProjectManager.LightBox({
							"message": '<h2>Uh Oh!</h2><div class="light-box-message">Something happened when trying to delete the record. Please try again and/or tell Brenton about this and give him the following error code:<br /><br/><span style="color: #990000;">"' + error.code + ': ' + error.message + '"</span></div>',
							"confirm": function() {},
							"cancel": function() {},
							"showCancel": false
						});
					});
				break;
			}
		}

	};

	var ProjectManager = {

		// PROPERTIES
		"days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
		"months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
		"daysInMonth": [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
		"toolbarIsSticky": false,
		"calHeaderIsSticky": false,
		"eventsNotBound": true,
		"firebaseDB": FirebaseConfiguration.Config({
			"apiKey": "AIzaSyBOGd_HYCyPkqBGiSJ7Z6JPPeU5ORWaSfY",
			"authDomain": "dev-project-manager-5bad7.firebaseapp.com",
			"databaseURL": "https://dev-project-manager-5bad7.firebaseio.com/",
			"storageBucket": ""
		}),

		// METHODS
		"Init": function() {
			// FOR SCOPE
			var _this = this;

			// BIND SCROLL/RESIZE EVENTS
			$(window).resize(function() { _this.StickyElements(); });
			$(window).scroll(function() { _this.StickyElements(); });
			this.StickyElements();

			// GET THINGS STARTED
			var firebaseData = FirebaseConfiguration.GetData({
				"firebaseApp": this.firebaseDB,
				"ref": "/",
				"ready": function(snapshot) {
					// BUILD THE VIEW
					_this.BuildView(snapshot);
				}
			});
		},

		"StickyElements": function() {
			// TOOLBAR
			var toolbarPos = $("#header-toolbar-outer").offset().top - 56;
			if ($(window).scrollTop() <= toolbarPos) {
				if(this.toolbarIsSticky) {
					$(".section.main").removeClass("sticky-toolbar");

					this.toolbarIsSticky = false;
				}
			} else {
				if(!this.toolbarIsSticky) {
					$(".section.main").addClass("sticky-toolbar");

					this.toolbarIsSticky = true;
				}
			}

			// CALENDAR HEADERS
			var calHeaderPos = $("#content").offset().top - 131;
			if ($(window).scrollTop() <= calHeaderPos) {
				if(this.calHeaderIsSticky) {
					$(".calendar-sticky-header").removeClass("sticky-header").css("top", "auto");

					this.calHeaderIsSticky = false;
				}
			} else {
				if(!this.calHeaderIsSticky) {
					$(".calendar-sticky-header").addClass("sticky-header");

					this.calHeaderIsSticky = true;
				}

				$(".calendar-sticky-header").css("top", $(window).scrollTop());
			}
		},

		"BuildView": function(snapshot) {
			// GET THE PROJECTS
			var projects = snapshot.child("/projects").val();

			// CHECK AUTHENTICATION
			if(FirebaseConfiguration.IsAuth(this.firebaseDB)) {
				$(".button.settings, .button.manage, .button.logout, .button.past-projects").removeClass("hidden");
				
				/*var userName = this.firebaseDB.auth().currentUser.email.replace("@blackboard.com", "").split(".");
				var firstName = '<span class="first-name">' + userName[0] + '</span>';
				var randomGreeting = [
					"Oh hey " + firstName + ", do you think you might actually do some work today?",
					"Oh, it's you again.",
					"Great, it's you again.",
					"Hey " + firstName + ", working hard or hardly working?",
					"What do you want this time, " + firstName + "?",
					"So " + firstName + ", do you actually do any work around here?",
					"Hey " + firstName + ", Ssshhhhhhhhhhh.",
					"Hey " + firstName + ", I see you're pretending to do some work again."
				];
				if(!$(".section.header p").length) {
					$(".section.header").append('<p>' + randomGreeting[Math.floor((Math.random() * randomGreeting.length))] + '</p>');
				}*/
			} else {
				$(".button.login, .button.past-projects").removeClass("hidden");	
			}
			
			// ADD QUEUE TIME
			var queueTime = snapshot.child("/queueTime").val();
			if(!$("#current-timeline").length) {
				$(".header-section.right").prepend('<p id="current-timeline">Current Development Timeline: ' + queueTime + '</p>');
			}

			// CHECK WEEKENDS FILTER
			if(this.ReadCookie("weekends")) {
				$(".button.weekends").addClass("show").removeClass("hidden");
			} else {
				$(".button.weekends").addClass("hide").removeClass("hidden");
			}
			
			// CHECK TRM PROJECTS FILTER
			if(this.ReadCookie("trmProjects")) {
				$(".button.trm-projects").addClass("show").removeClass("hidden");
			} else {
				$(".button.trm-projects").addClass("hide").removeClass("hidden");
			}

			// SET UP OUR VARIABLES
			var todayDate = new Date();
			todayDate.setHours(0, 0, 0, 0);

			// SET MONTH AND YEAR
			var month = todayDate.getMonth();
			var year = todayDate.getFullYear();

			// SET MONTH RANGE
			var defaultFutureMonth = (this.ReadCookie("monthRange")) ? parseInt(this.ReadCookie("monthRange")) : 3;
			$("#month-range").val(defaultFutureMonth);
			var today = new Date();
			today.setHours(0, 0, 0, 0);
			var futureDate = new Date(today.setMonth(today.getMonth() + defaultFutureMonth));
			futureDate.setHours(0, 0, 0, 0);

			// LOOP THROUGH THE DATA SET
			// MAP THE DATA TO AN ARRAY SO THAT WE CAN SORT IT
			var dataArray = [], arrayRecord = "";;
			$.each(projects, function(id, record) {
				var arrayRecord = {id: id};

				$.each(record, function(key, value) {
					arrayRecord[key] = value;
				});

				dataArray.push(arrayRecord);
			});

			// SORT THE ARRAY
			dataArray.sort(function(a, b) {
				// SORT BY RESOURCE
				if(a.resource < b.resource) {
					return -1;
				}
				if(a.resource > b.resource) {
					return 1;
				}

				// THEN SORT BY START DATE
				var aDate = new Date(a.startDate);
				var bDate = new Date(b.startDate);
				if(aDate < bDate) {
					return -1;
				}
				if(aDate > bDate) {
					return 1;
				}
				return 0;
			});

			// BUILD HOLLIDAY DATE ARRAY
			var holidayDates = [], holidayStartDate = "", holidayEndDate = "";
			var holidays = snapshot.child("/holidays").val();

			// LOOP THROUGH THE HOLIDAYS
			for(var h in holidays) {
				holidayStartDate = new Date(holidays[h].startDate);
				holidayEndDate = new Date(holidays[h].endDate);

				// IF THIS HOLIDAY HAS A DATE RANGE
				if(holidayStartDate != holidayEndDate) {
					while(holidayStartDate <= holidayEndDate) {
						// PUSH HOLIDAY DATE TO holidayDates ARRAY
						holidayDates.push(holidayStartDate.getTime());

						// ADD 1 DAY TO HOLIDAY START DATE FOR THE NEXT LOOP
						holidayStartDate.setDate(holidayStartDate.getDate() + 1);
					}
				}

				// IF THIS HOLIDAY IS ONE DAY
				else {
					// PUSH HOLIDAY DATE TO holidayDates ARRAY
					holidayDates.push(holidayStartDate.getTime());
				}
			}

			// ADD EVERYTHING TO THE DOM
			$("#calendar-record-info-panel").html(this.GetInfoPanel(dataArray)).removeClass("hidden");
			
			var dayHeaders = this.GetCalendarHeaders({
				"endDate": futureDate,
				"template": '<div class="calendar-cell day">[-DAY-]</div>'
			});
			var dateHeaders = this.GetCalendarHeaders({
				"endDate": futureDate,
				"template": '<div class="calendar-cell date">[-DATE-]</div>'
			});
			var calendarRows = this.GetCalendarRows({
				"dataArray": dataArray,
				"holidays": holidayDates,
				"endDate": futureDate,
				"template": {
					"resourceDivider": '<div class="calendar-record resource-divider"></div>',
					"calendarRow": '<div class="calendar-record clear">[-CELLS-]</div>',
					"projectCell": '<div class="calendar-cell record project[-SPECIAL-CASE-CLASSES-]" data-account-name="[-PROJECT-ACCOUNT-NAME-]" data-project-type="[-PROJECT-TYPE-]" data-resource="[-PROJECT-RESOURCE-]" data-designer="[-PROJECT-DESIGNER-]" data-date-range="[-PROJECT-START-DATE-] - [-PROJECT-END-DATE-]" data-qa-date="[-PROJECT-QA-DATE-]" data-delivery-date="[-PROJECT-DELIVERY-DATE-]" data-phase="[-PROJECT-PHASE-]" data-status="[-PROJECT-STATUS-]" data-salesforce-project="[-PROJECT-SALESFORCE-URL-]" data-trello-card="[-PROJECT-TRELLO-CARD-URL-]"></div>',
					"holidayCell": '<div class="calendar-cell record holiday"></div>',
					"weekendCell": '<div class="calendar-cell record weekend"></div>',
					"ptoCell": '<div class="calendar-cell record flex"></div>',
					"emptyCell": '<div class="calendar-cell record empty"></div>'
				}
			});
			
			var calendarContent  =	'<div class="calendar-sticky-header">' +
										'<div id="calendar-day-headers" class="clear">' +
											dayHeaders.compiledHeaders +
										'</div>' +
										'<div id="calendar-date-headers" class="clear">' +
											dateHeaders.compiledHeaders +
										'</div>' +
									'</div>' +
									calendarRows.compiledRows;
				
			$("#content-inner").html(calendarContent).removeClass("hidden");
			$("#content").removeClass("loading");

			// BUILD OUT THE FILTER OPTIONS AND ADD THEM TO THE DOM TOO
			var resourceGroup = calendarRows.resources;
			var resourceOptions = "<option>Filter Resource</option>";
			for(var d in resourceGroup) {
				resourceOptions += '<option value="' + resourceGroup[d] + '">' + resourceGroup[d] + '</option>';
			}
			$("#filter-options").html(resourceOptions);

			// CHECK FOR FILTER
			if(this.ReadCookie("filter")) {
				$(".button.remove-filter").removeClass("hidden");
			} else {
				$("#filter-options").removeClass("hidden");
			}

			// BIND EVENTS
			this.UiEvents(dataArray);
			
			// ATTACH ADMIN UI IF AUTHENTICATED
			if(FirebaseConfiguration.IsAuth(this.firebaseDB)) {
				ProjectManagerAdmin.Init({
					"database": this.firebaseDB,
					"data": dataArray,
					"snapshot": snapshot
				});
				
				// KEPT THIS IN CASE I WANT TO USE IT AGAIN LATER
				/*ProjectManagerAdmin.HouseCleaning({
					"database": this.firebaseDB,
					"projects": projects
				});*/
			}
		},
		
		"GetCalendarHeaders": function(options) {
			var headers = '',
			    headerLoopDate = new Date(),
			    futureDate = options.endDate,
				isWeekend = false,
				dayNum = 0;
				
			headerLoopDate.setHours(0, 0, 0, 0);
			
			while(headerLoopDate <= futureDate) {
				isWeekend = (headerLoopDate.getDay() == 0 || headerLoopDate.getDay() == 6);
			
				// CHECK WEEKENDS
				if((!isWeekend) || (isWeekend && !this.ReadCookie("weekends"))) {
					headers +=	options.template
								.replace(/\[\-day\-\]/ig, this.days[headerLoopDate.getDay()])
								.replace(/\[\-date\-\]/ig, (headerLoopDate.getMonth() + 1) + '/' + headerLoopDate.getDate() + '/' + headerLoopDate.getFullYear());

					dayNum++;
				}

				headerLoopDate.setDate(headerLoopDate.getDate() + 1);
			}
			
			return {
				"compiledHeaders": headers,
				"dayNum": dayNum
			}
		},
		
		"GetCalendarRows": function(options) {
			var dataArray = options.dataArray,
			    holidayDates = options.holidays,
				futureDate = options.endDate,
			    recordRows = '',
			    recordCells = '',
			    resource = '',
				resourceGroup = [],
				todayDate = new Date();
					
			todayDate.setHours(0, 0, 0, 0);

			// LOOP THROUGH ALL OF THE RECORDS
			for(var r in dataArray) {
				var deliveryDate = new Date(dataArray[r].deliveryDate);
				deliveryDate.setHours(0, 0, 0, 0);
				recordCells = '';
				
				// CHECK FOR FILTER
				// MAKE SURE RECORD ISNT EXEMPT
				// DONT GET RECORD IF DELIVERY DATE IS OLDER THAN TODAY
				// DONT GET RECORD IF TRM PROJECTS ARE SET TO HIDE AND THIS LOOP IS A TRM PROJECT
				if(!this.ReadCookie("filter") && dataArray[r].status != "Exempt" && (deliveryDate == "Invalid Date" || deliveryDate >= todayDate) || this.ReadCookie("filter") && dataArray[r].resource == this.ReadCookie("filter") && dataArray[r].status != "Exempt" && (deliveryDate == "Invalid Date" || deliveryDate >= todayDate)) {
					if(!this.ReadCookie("trmProjects") || this.ReadCookie("trmProjects") && (dataArray[r].projectType != "Maintenance Package - Template" && dataArray[r].projectType != "Maintenance Package - App")) {
						var recordLoopDate = new Date();
						recordLoopDate.setHours(0, 0, 0, 0);
						var startDate = new Date(dataArray[r].startDate);
						startDate.setHours(0, 0, 0, 0);
						var endDate = new Date(dataArray[r].endDate);
						endDate.setHours(0, 0, 0, 0);
						var qaDate = new Date(dataArray[r].qaDate);
						qaDate.setHours(0, 0, 0, 0);
						var additionalClasses = "";

						// CHECK THE CURRENT RESOURCE AND ADD A DIVIDER IF NEEDED
						if(resource == "") {
							resource = dataArray[r].resource;

							// ADD THIS RESOURCE TO THE RESOURCE GROUP TO BUILD OUT THE FILTER OPTIONS
							resourceGroup.push(resource);
						} else if(dataArray[r].resource != resource) {
							resource = dataArray[r].resource;
							recordRows += options.template.resourceDivider;

							// ADD THIS RESOURCE TO THE RESOURCE GROUP TO BUILD OUT THE FILTER OPTIONS
							resourceGroup.push(resource);
						}

						// LOOP THROUGH EACH DAY FOR THIS RECORD
						while(recordLoopDate <= futureDate) {
							var accountNameCheck = dataArray[r].accountName.toLowerCase();
							additionalClasses = "";

							if(dataArray[r].status == "Tentative") {
								additionalClasses += " tentative";
							}

							if(startDate.getTime() == recordLoopDate.getTime()) {
								additionalClasses += " start";
							}

							if(endDate.getTime() == recordLoopDate.getTime()) {
								additionalClasses += " end";
							}

							if(qaDate.getTime() == recordLoopDate.getTime()) {
								additionalClasses += " qa";
							}

							if(deliveryDate.getTime() == recordLoopDate.getTime()) {
								additionalClasses += " deliver";
							}

							// recordLoopDate IS A HOLLIDAY
							if(holidayDates.indexOf(recordLoopDate.getTime()) > -1) {
								// CHECK IF HOLIDAY IS ON WEEKEND
								if(recordLoopDate.getDay() == 0 || recordLoopDate.getDay() == 6) {
									// ONLY SHOW THE WEEKEND HOLIDAY IF WEEKENDS ARE SET TO SHOW
									if(!this.ReadCookie("weekends")) {
										recordCells += options.template.holidayCell;
									}
								} else {
									recordCells += options.template.holidayCell;	
								}
							}

							// recordLoopDate IS A WEEKEND DAY
							else if(recordLoopDate.getDay() == 0 || recordLoopDate.getDay() == 6) {
								if(!this.ReadCookie("weekends")) {
									recordCells += options.template.weekendCell;
								}
							}

							// recordLoopDate IS A FLEX DAY
							else if((accountNameCheck == "flex time" || accountNameCheck == "pto") && recordLoopDate >= startDate && recordLoopDate <= endDate) {
								recordCells += options.template.ptoCell;	
							}

							// recordLoopDate IS A WEEKDAY
							else {
								// WE HAVE DATA FOR THIS DAY
								if(recordLoopDate >= startDate && recordLoopDate <= deliveryDate) {

									if(recordLoopDate >= qaDate && recordLoopDate < deliveryDate) {
										additionalClasses += " qa-duration";
									}

									recordCells +=	options.template.projectCell
													.replace(/\[\-special\-case\-classes\-\]/ig, additionalClasses)
													.replace(/\[\-project\-account\-name\-\]/ig, dataArray[r].accountName)
													.replace(/\[\-project\-type\-\]/ig, dataArray[r].projectType)
													.replace(/\[\-project\-resource\-\]/ig, dataArray[r].resource)
													.replace(/\[\-project\-designer\-\]/ig, dataArray[r].designer)
													.replace(/\[\-project\-start\-date\-\]/ig, dataArray[r].startDate)
													.replace(/\[\-project\-end\-date\-\]/ig, dataArray[r].endDate)
													.replace(/\[\-project\-qa\-date\-\]/ig, dataArray[r].qaDate)
													.replace(/\[\-project\-delivery\-date\-\]/ig, dataArray[r].deliveryDate)
													.replace(/\[\-project\-phase\-\]/ig, dataArray[r].phase)
													.replace(/\[\-project\-status\-\]/ig, dataArray[r].status)
													.replace(/\[\-project\-salesforce\-url\-\]/ig, dataArray[r].salesforceProject)
													.replace(/\[\-project\-trello\-card\-url\-\]/ig, dataArray[r].trelloCard);
								}

								// WE HAVE NO DATA SO THIS DAY IS EMPTY
								else {
									recordCells += options.template.emptyCell;
								}
							}

							// ADD 1 DAY TO LOOP DATE FOR THE NEXT LOOP
							recordLoopDate.setDate(recordLoopDate.getDate() + 1);
						}

						recordRows += options.template.calendarRow.replace(/\[\-cells\-\]/ig, recordCells);
					}
				}
			}
			
			return {
				"compiledRows": recordRows,
				"resources": resourceGroup
			}
		},

		"GetInfoPanel": function(dataArray) {
			var today = new Date();
			today.setHours(0, 0, 0, 0);
			var resource = "";
			var filter = (this.ReadCookie("filter")) ? "<strong>Filtering projects for:</strong> " + this.ReadCookie("filter") : "";
			var recordInfoPanel = '<div class="calendar-sticky-header"><div class="calendar-cell panel-empty width-100 no-float">' + filter + '</div><div class="calendar-panel-row clear"><div class="calendar-cell panel-header width-54 account-name">Account Name</div><div class="calendar-cell panel-header width-18 project-type">Type</div><div class="calendar-cell panel-header width-28 resource">Resource</div></div></div>';	

			// LOOP THROUGH THE RECORDS
			for(var r in dataArray) {
				var deliveryDate = new Date(dataArray[r].deliveryDate);

				// CHECK FOR FILTER, MAKE SURE RECORD ISNT EXEMPT AND DONT GET RECORD IF DELIVERY DATE IS OLDER THAN TODAY
				if(!this.ReadCookie("filter") && dataArray[r].status != "Exempt" && (deliveryDate == "Invalid Date" || deliveryDate >= today) || this.ReadCookie("filter") && dataArray[r].resource == this.ReadCookie("filter") && dataArray[r].status != "Exempt" && (deliveryDate == "Invalid Date" || deliveryDate >= today)) {
					if(!this.ReadCookie("trmProjects") || this.ReadCookie("trmProjects") && (dataArray[r].projectType != "Maintenance Package - Template" && dataArray[r].projectType != "Maintenance Package - App")) {
						var projectType = dataArray[r].projectType;
						if(dataArray[r].projectType.match(/(Template|Custom)/)) {
							projectType = $.trim(projectType.replace(/(Template|Custom)/g, ""));
						}

						// CHECK THE CURRENT RESOURCE AND ADD A DIVIDER IF NEEDED
						if(resource == "") {
							resource = dataArray[r].resource;
						} else if(dataArray[r].resource != resource) {
							resource = dataArray[r].resource;
							recordInfoPanel += '<div class="calendar-panel-row resource-divider"></div>';
						}

						// WRAP ACCOUNT NAME IN LINK IF THERE IS ONE AVAILABLE
						var salesForceLink = dataArray[r].salesforceProject;
						var salesForceLinks = dataArray[r].salesforceProject.split(",");
						if(salesForceLinks.length > 1) {
							salesForceLink = salesForceLinks[0];
						}
						var accountName = (dataArray[r].salesforceProject == "") ? dataArray[r].accountName : '<a href="' + salesForceLink + '" target="_blank">' + dataArray[r].accountName + '</a>';	

						// BUILD THE ROWS AND CELLS
						recordInfoPanel += '<div class="calendar-panel-row clear">';
						recordInfoPanel += 	'<div class="calendar-cell panel-info account-name width-54">' + accountName + '</div>';
						recordInfoPanel += 	'<div class="calendar-cell panel-info project-type width-18">' + projectType + '</div>';
						recordInfoPanel += 	'<div class="calendar-cell panel-info resource width-28">' + dataArray[r].resource + '</div>';
						recordInfoPanel += '</div>';
					}
				}
			}

			return recordInfoPanel;
		},

		"UiEvents": function(projectArray) {
			// FOR SCOPE
			var _this = this;
			
			if(this.eventsNotBound) {
				// FILTER CHANGE
				$(document).on("change", "#filter-options", function() {
					// SET THE FILTER COOKIE
					_this.CreateCookie("filter", $("option:selected", this).val(), 365);

					// UPDATE THE TOOLBAR
					$(".button.remove-filter").removeClass("hidden");
					$("#filter-options").addClass("hidden");

					// RELOAD THE PAGE
					location.reload(true);
				});

				// MONTH RANGE CHANGE
				$(document).on("focus", "#month-range", function() {
					$(this).val("");
				});

				$(document).on("change", "#month-range", function() {
					var newMonthRange = $.trim($(this).val());
					$(".cal-error").remove();

					if(_this.IsInteger(newMonthRange)) {
						// MAKE SURE THE NEW MONTH RANGE ISN'T BEYOND A YEAR
						if(newMonthRange > 12) {
							$('<span class="cal-error">This value can\'t be more than 12!</span>').insertAfter($(this));
						}

						// MAKE SURE THE NEW MONTH RANGE ISN'T BELOW 1 MONTH
						else if(newMonthRange < 1) {
							$('<span class="cal-error">This value must at least be 1!</span>').insertAfter($(this));
						}

						// ALL IS GOOD SO SET THE MONTH RANGE COOKIE AND RELOAD THE PAGE
						else {
							// SET THE FILTER COOKIE
							_this.CreateCookie("monthRange", newMonthRange, 365);

							// RELOAD THE PAGE
							location.reload(true);
						}
					} else {
						$('<span class="cal-error">You must enter an integer!</span>').insertAfter($(this));
					}
				});

				// REMOVE FILTER
				$(document).on("click", ".button.remove-filter", function(e) {
					e.preventDefault();

					// DELETE THE FILTER COOKIE
					_this.EraseCookie("filter");

					// UPDATE THE TOOLBAR
					$(".button.remove-filter").addClass("hidden");
					$("#filter-options").removeClass("hidden");

					// RELOAD THE PAGE
					location.reload(true);
				});

				// FILTER WEEKENDS BUTTON
				$(document).on("click", ".button.weekends", function(e) {
					e.preventDefault();

					if($(this).hasClass("hide")) {
						// SET THE WEEKENDS COOKIE
						_this.CreateCookie("weekends", false, 1825); // 1825 = 5 years
					} else {
						// ERASE THE WEEKENDS COOKIE
						_this.EraseCookie("weekends");
					}

					// RELOAD THE PAGE
					location.reload(true);
				});
				
				// FILTER TRM PROJECTS BUTTON
				$(document).on("click", ".button.trm-projects", function(e) {
					e.preventDefault();

					if($(this).hasClass("hide")) {
						// SET THE TRM PROJECTS COOKIE
						_this.CreateCookie("trmProjects", false, 1825); // 1825 = 5 years
					} else {
						// ERASE THE TRM PROJECTS COOKIE
						_this.EraseCookie("trmProjects");
					}

					// RELOAD THE PAGE
					location.reload(true);
				});

				// CLOSE OVERLAY
				$(document).on("click", ".overlay-bg", function() {
					$(this).fadeOut();
				});
				$(document).on("click", ".overlay-outer", function(e) {
					e.stopPropagation();
				});
				$(document).on("click", ".overlay-close", function(e) {
					e.preventDefault();

					$(this).parents(".overlay-bg").fadeOut();
				});

				// LOGIN BUTTON
				$(document).on("click", ".button.login", function(e) {
					e.preventDefault();

					$(".overlay-bg.login").fadeIn();
				});

				// SUBMIT LOGIN FORM
				$(document).on("submit", "#login-form", function(e) {
					e.preventDefault();

					$(".loader-working", this).show();

					var noErrors = true;
					$("#email, #password").removeAttr("style");
					if($.trim($("#email").val()) == "") {
						$("#email").css("border", "solid 1px #990000");

						noErrors = false;
					}
					if($.trim($("#password").val()) == "") {
						$("#password").css("border", "solid 1px #990000");

						noErrors = false;
					}

					if(noErrors) {
						// ATTEMPT AUTHENTICATION
						FirebaseConfiguration.Authentication({
							"database": _this.firebaseDB,
							"action": "login",
							"email": $("#email").val(),
							"password": $("#password").val()
						});
					} else {
						$(".loader-working", this).hide();

						return false;
					}
				});

				// TOOLTIP
				$(document).on("click", ".calendar-cell.record.project", function() {
					var toolTip = '';
					var detailsForCopy = '';

					// ACCOUNT NAME
					toolTip +=	'<p><strong>Account:</strong> ' + $(this).attr("data-account-name") + '</p>';
					detailsForCopy += 'ACCOUNT: ' + $(this).attr("data-account-name") + '\n';

					// PROJECT TYPE
					toolTip +=	'<p><strong>Project Type:</strong> ' + $(this).attr("data-project-type") + '</p>';
					detailsForCopy += 'PROJECT TYPE: ' + $(this).attr("data-project-type") + '\n';

					// DEVELOPER
					toolTip +=	'<p><strong>Developer:</strong> ' + $(this).attr("data-resource") + '</p>';
					detailsForCopy += 'DEVELOPER: ' + $(this).attr("data-resource") + '\n';

					// CHECK FOR DESIGNER INFO
					if($(this).attr("data-designer") && $(this).attr("data-designer") != "") {
						toolTip +=	'<p><strong>Designer:</strong> ' + $(this).attr("data-designer") + '</p>';
						detailsForCopy += 'DESIGNER: ' + $(this).attr("data-designer") + '\n';
					}

					// DEVELOPMENT TIMELINE
					toolTip +=	'<p><strong>Development Timeline:</strong> ' + $(this).attr("data-date-range") + '</p>';
					detailsForCopy += 'DEVELOPMENT TIMELINE: ' + $(this).attr("data-date-range");

					// CHECK FOR QA DATE
					if($(this).attr("data-qa-date") && $(this).attr("data-qa-date") != "") {
						toolTip +=	'<p><strong>QA Date:</strong> ' + $(this).attr("data-qa-date") + '</p>';
						detailsForCopy += '\nQA DATE: ' + $(this).attr("data-qa-date");
					}

					// CHECK FOR DELIVERY DATE
					if($(this).attr("data-delivery-date") && $(this).attr("data-delivery-date") != "") {
						toolTip +=	'<p><strong>Delivery Date:</strong> ' + $(this).attr("data-delivery-date") + '</p>';
						detailsForCopy += '\nDELIVERY DATE: ' + $(this).attr("data-delivery-date");
					}

					// CHECK FOR PROJECT STATUS
					if($(this).attr("data-status") && $(this).attr("data-status") != "") {
						toolTip +=	'<p><strong>Project Status:</strong> ' + $(this).attr("data-status") + '</p>';
						detailsForCopy += '\nPROJECT STATUS: ' + $(this).attr("data-status");
					}

					// CHECK FOR SALESFORCE PROJECT LINK(S)
					if($(this).attr("data-salesforce-project") && $(this).attr("data-salesforce-project") != "") {
						var salesForceLinks = $(this).attr("data-salesforce-project").split(",");
						var salesForceProjectTitle = "";
						var haveMultipleSalesForceProjects = false;
						if(salesForceLinks.length > 1) {
							haveMultipleSalesForceProjects = true;
						}
						for(var l in salesForceLinks) {
							if(haveMultipleSalesForceProjects) {
								salesForceProjectTitle = "SalesForce Project " + (parseInt(l) + 1);
							} else {
								salesForceProjectTitle = "SalesForce Project";
							}
							toolTip +=	'<p><strong>' + salesForceProjectTitle + ':</strong> <a href="' + $.trim(salesForceLinks[l]) + '" target="_blank">' + $.trim(salesForceLinks[l]) + '</a></p>';
						}
					}

					// CHECK FOR TRELLO CARD LINK
					if($(this).attr("data-trello-card") && $(this).attr("data-trello-card") != "") {
						toolTip +=	'<p><strong>Trello Card:</strong> <a href="' + $(this).attr("data-trello-card") + '" target="_blank">' + $(this).attr("data-trello-card") + '</a></p>';
					}
					
					// ADD COPY BUTTON
					toolTip += '<button class="button right" id="copy-tool-tip-details">Copy Details</button>';
					toolTip += '<textarea id="overview-details" style="width: 1px; height: 1px; border: 0; margin: 0px; padding: 0px; outline: none;">' + detailsForCopy + '</textarea>';

					$(".overlay-bg.tool-tip .overlay-inner").html(toolTip);
					$(".overlay-bg.tool-tip").fadeIn();
				});
				
				// TOOL TIP COPY DETAILS BUTTON
				$(document).on("click", "#copy-tool-tip-details", function(e) {
					e.preventDefault();
					ProjectManager.CopyToClipboard($("#overview-details").val());
					ProjectManager.Notify("Project details copied to clipboard!");
				});
				
				// PAST PROJECTS
				$(document).on("click", ".button.past-projects", function(e) {
					e.preventDefault();
					
					projectArray.sort(function(a, b) {
						// SORT BY RESOURCE
						if(a.resource < b.resource) {
							return -1;
						}
						if(a.resource > b.resource) {
							return 1;
						}

						// THEN SORT BY START DATE
						var aDate = new Date(a.deliveryDate);
						var bDate = new Date(b.deliveryDate);
						if(aDate > bDate) {
							return -1;
						}
						if(aDate < bDate) {
							return 1;
						}
						return 0;
					});
					
					var resource = "";
					var resourceCounter = 1;
					var numberOfProjects = 0;
					var today = new Date();
					today.setHours(0, 0, 0, 0);
					
					var resourceTabs = '';
					var projectPanels = '';
					
					// LOOP THROUGH THE RECORDS
					for(var r in projectArray) {
						var deliveryDate = new Date(projectArray[r].deliveryDate);
						deliveryDate.setHours(0, 0, 0, 0);

						// ONLY GRAB THE RECORD IF IT DELIVERED BEFORE TODAY
						if(deliveryDate.getTime() < today.getTime() && projectArray[r].status != "Exempt") {
							// CHECK THE CURRENT RESOURCE AND ADD A DIVIDER IF NEEDED
							if(resource == "" || projectArray[r].resource != resource) {
								if(resource != "") projectPanels += '</table></li>';
								resourceTabs += '<li class="resource-tab developer-' + resourceCounter + ((resource == "") ? ' active' : '') + '" data-resource-tab="developer-' + resourceCounter + '">' + projectArray[r].resource + '</li>';
								projectPanels += '<li class="project-panel developer-' + resourceCounter + ((resource == "") ? ' active' : '') + '"><table><tr><th>Account</th><th>Project</th><th>Designer</th><th>Delivery Date</th><th>Project Urls</th><th>Trello Card</th></tr>';
								
								resource = projectArray[r].resource;
								resourceCounter++;
							}

							var salesForceLinks = projectArray[r].salesforceProject.split(",");
							var haveMultipleSalesForceProjects = false;
							var salesForceProjects = '';
							if(salesForceLinks.length > 1) {
								salesForceProjects += '<ul>';
								
								haveMultipleSalesForceProjects = true;
							}
							for(var l in salesForceLinks) {
								if(haveMultipleSalesForceProjects) {
									salesForceProjects += '<li><a href="' + $.trim(salesForceLinks[l]) + '" target="_blank">SalesForce Project ' + (parseInt(l) + 1) + '</a></li>';
								} else {
									salesForceProjects += '<a href="' + $.trim(salesForceLinks[l]) + '" target="_blank">SalesForce Project</a>';
								}
							}
							if(salesForceLinks.length > 1) {
								salesForceProjects += '</ul>';
							}

							// BUILD THE ROW AND CELLS
							projectPanels +=	'<tr>';
							projectPanels +=		'<td>' + projectArray[r].accountName + '</td>';
							projectPanels +=		'<td>' + projectArray[r].projectType + '</td>';
							projectPanels +=		'<td>' + projectArray[r].designer + '</td>';
							projectPanels +=		'<td>' + projectArray[r].deliveryDate + '</td>';
							projectPanels +=		'<td>' + salesForceProjects + '</td>';
							projectPanels +=		'<td>' + ((projectArray[r].trelloCard != "") ? '<a href="' + projectArray[r].trelloCard + '" target="_blank">Trello Card</a>' : '') + '</td>';
							projectPanels +=	'</tr>';
						
							numberOfProjects++;
						}
					}
					
					var pastProjectsUI = '';
					if(numberOfProjects > 0) {
						pastProjectsUI +=	'<div id="past-projects-tabbed-panels">';
						pastProjectsUI += 		'<div id="past-projects-tabs"><ul>' + resourceTabs + '</u></div>';
						pastProjectsUI += 		'<div id="past-projects-panels"><ul>' + projectPanels + '</ul></div>';
						pastProjectsUI += 	'</div>';
					} else {
						pastProjectsUI = '<p>There are currently no past projects to show.</p>';
					}

					$(".overlay-bg.past-projects .overlay-inner").html(pastProjectsUI);
					$(".overlay-bg.past-projects").fadeIn();
					
					$("#past-projects-tabs").on("click", ".resource-tab", function() {
						$("#past-projects-tabs .resource-tab, #past-projects-panels .project-panel").removeClass("active");
						$(this).addClass("active");
						$("#past-projects-panels .project-panel." + $(this).attr("data-resource-tab")).addClass("active");
					});
				});
				
				this.eventsNotBound = false;
			}
		},

		"IsInteger": function(num) {
			return num % 1 === 0;
		},
		
		"LightBox": function(options) {
			$("#light-box-outer").stop(true, true);
	
			// CANCEL BUTTON
			if(options.showCancel) {
				$("#light-box-cancel").show();
			} else {
				$("#light-box-cancel").hide();
			}

			// ADD MESSAGE AND SHOW THE LIGHTBOX
			$("#light-box-outer #light-box-content").html(options.message);
			$("#light-box-outer").fadeIn();

			// CONFIRM
			$("#light-box-confirm").unbind().click(function(e) {
				e.preventDefault();
				if(options.confirm() != false) {
					$("#light-box-outer").fadeOut();
				}
			});

			// CANCEL
			$("#light-box-outer, #light-box-cancel").unbind().click(function(e) {
				e.preventDefault();
				options.cancel();
				$("#light-box-outer").fadeOut();
			});

			// STOP EVENT PROPAGATION OF OUTER CLICK
			$("#light-box").unbind().click(function(e) {
				e.stopImmediatePropagation();
			});
		},

		"Notify": function(msg) {
			$("#notify").text(msg).show();

			setTimeout(function() {
				$("#notify").fadeOut(function() {
					$(this).empty();
				});
			}, 2500);
		},

		"CreateCookie": function(name, value, days) {
			var expires;

			if (days) {
				var date = new Date();
				date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
				expires = "; expires=" + date.toGMTString();
			} else {
				expires = "";
			}
			document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
		},

		"ReadCookie": function(name) {
			var nameEQ = escape(name) + "=";
			var ca = document.cookie.split(';');
			for (var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) === ' ') c = c.substring(1, c.length);
				if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
			}
			return null;
		},

		"EraseCookie": function(name) {
			this.CreateCookie(name, "", -1);
		},
		
		"CopyToClipboard": function(text) {
			$("body").append($('<textarea id="copy-code-holder" style="width: 1px; height: 1px; border: 0; margin: 0px; padding: 0px; outline: none;">' + text + '</textarea>'));
			$("#copy-code-holder").select();
			document.execCommand("Copy", false, null);
			$("#copy-code-holder").remove();
		}

	};


	var ProjectManagerAdmin = {
		// PROPERTIES
		"projectManagerEventsNotBound": true,
		"settingsManagerEventsNotBound": true,
		
		// METHODS
		"Init": function(options) {
			// BUILD THE PROJECT MANAGER VIEW
			this.ProjectManagerBuildView(options);
			
			// BIND PROJECT MANAGER EVENTS
			this.ProjectManagerUiEvents(options);
			
			// BUILD THE SETTINGS MANAGER VIEW
			this.SettingsManagerBuildView(options);
			
			// BIND SETTINGS MANAGER EVENTS
			this.SettingsManagerUiEvents(options);
		},
		
		"ProjectManagerBuildView": function(options) {
			var snapshot = options.snapshot;
		
			// SORT THE DATA ARRAY BY JUST DELIVERY DATE
			var sortedData = options.data;
			sortedData.sort(function(a, b) {
				var dateA = new Date(a.deliveryDate);
				var dateB = new Date(b.deliveryDate);

				return dateA - dateB;
			});
			
			var today = new Date();
			today.setHours(0, 0, 0, 0);

			// LOOP THROUGH ALL OF THE RECORDS
			var currentProjects = '<div id="pm-current-records"><div class="pm-record pm-record-header currect clear"><h2>Current Projects</h2></div><div class="pm-records-list">';
			var pastProjects = '<div id="pm-past-records"><div class="pm-record pm-record-header past clear"><h2>Past Projects</h2></div><div class="pm-records-list hidden">';
			for(var r in sortedData) {
				var delivery = new Date(sortedData[r].deliveryDate);
				delivery.setHours(0, 0, 0, 0);
				var exempt = (sortedData[r].status == "Exempt") ? ' <span class="pm-exempt">(Exempt)</span>' : '';
				var deliveryDate = (sortedData[r].deliveryDate != "") ? ' - Delivery: ' + sortedData[r].deliveryDate + '</span>' : '</span>';
				var projectType = (sortedData[r].projectType!= "") ? ' - ' + sortedData[r].projectType : '';
				var thisProject = '';
				
				thisProject += '<div class="pm-record clear" data-record-id="' + sortedData[r].id + '">';
				thisProject += 	'<span class="left">' + sortedData[r].accountName + ' | <span class="pm-additional-info">' + sortedData[r].resource + projectType + deliveryDate + exempt + '</span>';
				thisProject += 	'<button class="right button delete pm-project-delete" data-record-id="' + sortedData[r].id + '">Delete</button>';
				thisProject += 	'<button class="right button edit edit-project"';
				thisProject += 	' data-record-id="' + sortedData[r].id + '"';
				thisProject += 	' data-account-name="' + sortedData[r].accountName + '"';
				thisProject += 	' data-project-type="' + sortedData[r].projectType + '"';
				thisProject += 	' data-resource="' + sortedData[r].resource + '"';
				thisProject += 	' data-designer="' + sortedData[r].designer + '"';
				thisProject += 	' data-start-date="' + sortedData[r].startDate + '"';
				thisProject += 	' data-end-date="' + sortedData[r].endDate + '"';
				thisProject += 	' data-qa-date="' + sortedData[r].qaDate + '"';
				thisProject += 	' data-delivery-date="' + sortedData[r].deliveryDate + '"';
				thisProject += 	' data-phase="' + sortedData[r].phase + '"';
				thisProject += 	' data-status="' + sortedData[r].status + '"';
				thisProject += 	' data-salesforce-project="' + sortedData[r].salesforceProject + '"';
				thisProject += 	' data-trello-card="' + sortedData[r].trelloCard + '"';
				thisProject += 	'>Edit</button>';
				thisProject += '</div>';
					
				if(delivery == "Invalid Date" || delivery >= today) {
					currentProjects += thisProject
				} else {
					pastProjects += thisProject;
				}
			}
			currentProjects += '</div></div>';
			pastProjects += '</div></div>';

			var projectManager =	'<div id="pm-record-toolbar" class="clear">' +
										'<button class="button new-project">New Project</button>' +
										'<input type="text" id="pm-quick-search" placeholder="Quick Search" />' +
										'<a href="#" class="button right generate-csv" download="projects.csv">Generate Data CSV</a>' +
									'</div>' +
									currentProjects +
									pastProjects;

			// IF EDITOR DOESN'T YET EXIST
			if(!$(".overlay-outer.project-manager .overlay-inner .project-editor").length) {
				// BUILD DEVELOPER LIST
				var developers = "";
				var developerList = snapshot.child("/developers").val().replace(/, /g, ",").split(",");
				developerList.sort();
				for(var d in developerList) {
					developers += '<option value="' + developerList[d] + '">' + developerList[d] + '</option>';	
				}

				// BUILD DESIGNER LIST
				var designers = "";
				var designerList = snapshot.child("/designers").val().replace(/, /g, ",").split(",");
				designerList.sort();
				for(var d in designerList) {
					designers += '<option value="' + designerList[d] + '">' + designerList[d] + '</option>';	
				}

				// BUILD PROJECT TYPES
				var projectTypes = "";
				var projectTypeList = snapshot.child("/projectTypes").val().replace(/, /g, ",").split(",");
				projectTypeList.sort();
				for(var p in projectTypeList) {
					projectTypes += '<option value="' + projectTypeList[p] + '">' + projectTypeList[p] + '</option>';	
				}

				// BUILD PHASE OPTIONS
				var phaseOptions = "";
				var phaseOptionsList = snapshot.child("/phases").val().replace(/, /g, ",").split(",");
				phaseOptionsList.sort();
				for(var s in phaseOptionsList) {
					phaseOptions += '<option value="' + phaseOptionsList[s] + '">' + phaseOptionsList[s] + '</option>';	
				}

				// BUILD STATUS OPTIONS
				var statusOptions = "";
				var statusOptionsList = snapshot.child("/status").val().replace(/, /g, ",").split(",");
				statusOptionsList.sort();
				for(var s in statusOptionsList) {
					statusOptions += '<option value="' + statusOptionsList[s] + '">' + statusOptionsList[s] + '</option>';	
				}
				
				projectManager += '<div class="project-editor clear">';
				projectManager += '	<input type="hidden" id="pm-record-id" class="pm-input" value="" />';
				projectManager += '	<div class="pm-control"><h4>Account Name</h4><input type="text" id="pm-account-name" class="pm-input required" value="" data-error="You must enter an account name!" /></div>';
				projectManager += '	<div class="pm-control">';
				projectManager += '		<h4>Project Type</h4>';
				projectManager += '		<select id="pm-project-type" class="pm-select">';
				projectManager += '			<option>Choose a Project Type</option>';
				projectManager += 				projectTypes;
				projectManager += '		</select>';
				projectManager += '	</div>';
				projectManager += '	<div class="pm-control">';
				projectManager += '		<h4>Developer</h4>';
				projectManager += '		<select id="pm-resource" class="pm-select required" data-error="You must choose a resource!">';
				projectManager += '			<option>Choose a Developer</option>';
				projectManager += 				developers;
				projectManager += '		</select>';
				projectManager += '	</div>';
				projectManager += '	<div class="pm-control">';
				projectManager += '		<h4>Designer</h4>';
				projectManager += '		<select id="pm-designer" class="pm-select">';
				projectManager += '			<option>Choose a Designer</option>';
				projectManager += 				designers;
				projectManager += '		</select>';
				projectManager += '	</div>';
				projectManager += '	<div class="pm-control"><h4>Start Date</h4><input type="text" class="date-picker pm-input required" id="pm-start-date" value="" data-error="You must enter a start date!" /></div>';
				projectManager += '	<div class="pm-control"><h4>End Date</h4><input type="text" class="date-picker pm-input required" id="pm-end-date" value="" data-error="You must enter an end date!" /></div>';
				projectManager += '	<div class="pm-control"><h4>QA Date</h4><input type="text" class="date-picker pm-input" id="pm-qa-date" value="" /></div>';
				projectManager += '	<div class="pm-control"><h4>Delivery Date</h4><input type="text" class="date-picker pm-input required" id="pm-delivery-date" value=""value="" data-error="You must enter a delivery date!" /></div>';
				projectManager += '	<div class="pm-control">';
				projectManager += '		<h4>Phase</h4>';
				projectManager += '		<select id="pm-phase" class="pm-select">';
				projectManager += '			<option>Choose a Phase</option>';
				projectManager += 				phaseOptions;
				projectManager += '		</select>';
				projectManager += '	</div>';
				projectManager += '	<div class="pm-control">';
				projectManager += '		<h4>Status</h4>';
				projectManager += '		<select id="pm-status" class="pm-select">';
				projectManager += '			<option>Choose a Status</option>';
				projectManager += 				statusOptions;
				projectManager += '		</select>';
				projectManager += '	</div>';
				projectManager += '	<div class="pm-control"><h4>SalesForce Project</h4><input type="text" id="pm-salesforce-project" class="pm-input" value="" /></div>';
				projectManager += '	<div class="pm-control"><h4>Trello Card</h4><input type="text" id="pm-trello-card" class="pm-input" value="" /></div>';
				projectManager += '	<button class="button right edit pm-close-editor">Close</button>';
				projectManager += '	<button class="button right general pm-copy-details" id="pm-copy-details">Copy Email Details</button>';
				projectManager += '	<button class="button right general pm-copy-trello-details" id="pm-copy-trello-details">Copy Trello Details</button>';
				projectManager += '	<button class="button right pm-save-project">Save</button>';
				projectManager += '</div>';

				// ADD EVERYTHING TO THE DOM
				$(".overlay-outer.project-manager .overlay-inner").html(projectManager);
			} else {
				// REMOVE THE CURRENT RECORDS
				$("#pm-record-toolbar, #pm-current-records, #pm-past-records").remove();

				// ADD EVERYTHING TO THE DOM
				$(".overlay-outer.project-manager .overlay-inner").prepend(projectManager);
			}

			// BIND DATE PICKER
			$(".date-picker").datepicker();
		},
		
		"ProjectManagerUiEvents": function(options) {
			if(this.projectManagerEventsNotBound) {
				$(document).on("click", ".button.logout", function(e) {
					e.preventDefault();

					// LOG OUT
					FirebaseConfiguration.Authentication({
						"database": options.database,
						"action": "logout"
					});
				});

				// PROJECT MANAGER BUTTON
				$(document).on("click", ".button.manage", function(e) {
					e.preventDefault();

					$(".overlay-bg.project-manager").fadeIn();

					// BIND COPY EMAIL PROJECT DETAILS FUNCTIONALITY
					$(".overlay-bg.project-manager").on("click", ".pm-copy-details", function(e) {
						e.preventDefault();

						var projectType = ($("#pm-project-type option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-project-type option:selected").val();
						var resource = ($("#pm-resource option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-resource option:selected").val();
						var designer = ($("#pm-designer option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-designer option:selected").val();
						var phase = ($("#pm-phase option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-phase option:selected").val();
						var status = ($("#pm-status option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-status option:selected").val();
						var salutation = resource.split(" ");

						// BUILD OUT THE PROJECT DETAILS FOR EMAIL
						var projectDetails = "Hey " + salutation[0] + ",\n";
						projectDetails += "I just assigned you a " + projectType + " project for " + $("#pm-account-name").val() + ". ";

						// CHECK FOR DESIGNER
						if(designer != "") {
							projectDetails += designer + " is the designer on the project. ";
						}

						// PROJECT START DATE
						projectDetails += "I have you marked to start the project on " + $("#pm-start-date").val() + ". ";

						// CHECK FOR QA DATE
						if($("#pm-qa-date").val() != "") {
							projectDetails += "QA is set for " + $("#pm-qa-date").val() + " and delivery is set for " + $("#pm-delivery-date").val() + ". ";
						} else {
							projectDetails += "Delivery is set for " + $("#pm-delivery-date").val() + ". ";
						}

						// GATHER SALESFORCE PROJECT LINK(S)
						if($("#pm-salesforce-project").val() != "") {
							var salesForceLinks = $("#pm-salesforce-project").val().split(",");
							var salesForceProjectTitle = "";
							var haveMultipleSalesForceProjects = false;
							if(salesForceLinks.length > 1) {
								haveMultipleSalesForceProjects = true;
							}
						}

						// SALESFORCE PROJECT URL AND TRELLO CARD URL
						if($("#pm-salesforce-project").val() != "" && $("#pm-trello-card").val() != "") {
							projectDetails += "\n\n";

							for(var l in salesForceLinks) {
								if(haveMultipleSalesForceProjects) {
									salesForceProjectTitle = "SalesForce Project " + (parseInt(l) + 1);
								} else {
									salesForceProjectTitle = "SalesForce Project";
								}
								projectDetails += salesForceProjectTitle + ": " + $.trim(salesForceLinks[l]) + ".\n";
							}
							projectDetails += "Trello Card: " + $("#pm-trello-card").val() + ". ";
						}

						// SALESFORCE PROJECT URL BUT NO TRELLO CARD URL
						else if($("#pm-salesforce-project").val() != "" && $("#pm-trello-card").val() == "") {
							projectDetails += "\n\n";

							for(var l in salesForceLinks) {
								if(haveMultipleSalesForceProjects) {
									salesForceProjectTitle = "SalesForce Project " + (parseInt(l) + 1);
								} else {
									salesForceProjectTitle = "SalesForce Project";
								}
								projectDetails += salesForceProjectTitle + ": " + $.trim(salesForceLinks[l]) + ".";

								if(l != salesForceLinks.length - 1) {
									projectDetails += "\n";
								}
							}
						}

						// NO SALESFORCE PROJECT URL BUT TRELLO CARD URL
						else if($("#pm-salesforce-project").val() == "" && $("#pm-trello-card").val() != "") {
							projectDetails += "\n\nTrello Card: " + $("#pm-trello-card").val() + ". ";
						}

						// PROJECT MANAGER LINK
						projectDetails += "\n\nYou can check out the project manager for more info: http://bkelly.schoolwires.net/projects. Let me know if you have any questions.";

						ProjectManager.CopyToClipboard(projectDetails);

						ProjectManager.Notify("Email details copied to clipboard!");
					});

					// BIND COPY TRELLO PROJECT DETAILS FUNCTIONALITY
					$(".overlay-bg.project-manager").on("click", ".pm-copy-trello-details", function(e) {
						e.preventDefault();

						var projectType = ($("#pm-project-type option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-project-type option:selected").val();
						var resource = ($("#pm-resource option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-resource option:selected").val();
						var designer = ($("#pm-designer option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-designer option:selected").val();
						var phase = ($("#pm-phase option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-phase option:selected").val();
						var status = ($("#pm-status option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-status option:selected").val();
						var salutation = resource.split(" ");

						var salesForceProjects = "";
						if($("#pm-salesforce-project").val() != "") {
							var salesForceLinks = $("#pm-salesforce-project").val().split(",");
							var salesForceProjectTitle = "";
							var haveMultipleSalesForceProjects = false;
							if(salesForceLinks.length > 1) {
								haveMultipleSalesForceProjects = true;
							}
							for(var l in salesForceLinks) {
								if(haveMultipleSalesForceProjects) {
									salesForceProjectTitle = "SalesForce Project " + (parseInt(l) + 1);
								} else {
									salesForceProjectTitle = "SalesForce Project";
								}
								salesForceProjects += "**" + salesForceProjectTitle + ":** " + $.trim(salesForceLinks[l]) + "\n";
							}
						}

						// BUILD OUT THE PROJECT DETAILS FOR EMAIL
						var projectDetails = salesForceProjects;
						projectDetails += "**Primary Contact:** \n";
						projectDetails += "**Website URL:** \n";
						projectDetails += "**CSR:** \n\n";
						projectDetails += "**Project Notes:** \n";

						ProjectManager.CopyToClipboard(projectDetails);

						ProjectManager.Notify("Trello details copied to clipboard!");
					});
				});

				// EDIT PROJECT BUTTON
				$(document).on("click", ".button.edit-project", function(e) {
					e.preventDefault();

					if(!$(this).hasClass("open")) {
						$(".pm-input").val("");
						$(".pm-select option").removeAttr("selected");

						$("#pm-record-id").val($(this).attr("data-record-id"));
						$("#pm-account-name").val($(this).attr("data-account-name"));
						$("#pm-project-type option[value=\"" + $(this).attr("data-project-type") + "\"]").prop("selected", true);
						$("#pm-resource option[value=\"" + $(this).attr("data-resource") + "\"]").prop("selected", true);
						$("#pm-designer option[value=\"" + $(this).attr("data-designer") + "\"]").prop("selected", true);
						$("#pm-start-date").val($(this).attr("data-start-date"));
						$("#pm-end-date").val($(this).attr("data-end-date"));
						$("#pm-qa-date").val($(this).attr("data-qa-date"));
						$("#pm-delivery-date").val($(this).attr("data-delivery-date"));
						$("#pm-phase option[value=\"" + $(this).attr("data-phase") + "\"]").prop("selected", true);
						$("#pm-status option[value=\"" + $(this).attr("data-status") + "\"]").prop("selected", true);

						$("#pm-salesforce-project").val($(this).attr("data-salesforce-project"));
						$("#pm-trello-card").val($(this).attr("data-trello-card"));
						
						$(".overlay-bg.project-manager").animate({
							scrollTop: 0
						}, 500);
					}

					$(".overlay-bg.project-manager .overlay-inner").toggleClass("open");
				});

				// DELETE PROJECT
				$(document).on("click", ".pm-project-delete", function(e) {
					e.preventDefault();
					var recordID = $(this).attr("data-record-id");

					// CONFIRM BEFORE WE DELETE
					ProjectManager.LightBox({
						"message": '<h2>Hold on a tick!</h2><div class="light-box-message">Are you sure you want to delete this record?</div>',
						"confirm": function() {
							FirebaseConfiguration.Action({
								"action": "delete",
								"firebaseApp": options.database,
								"ref": "/projects/",
								"recordID": recordID,
								"recordData": "",
								"successMsg": "The record was deleted!"
							});
						},
						"cancel": function() {},
						"showCancel": true
					});
				});

				// NEW PROJECT BUTTON
				$(document).on("click", ".button.new-project", function(e) {
					e.preventDefault();

					$(".pm-input").val("");
					$(".pm-select option").removeAttr("selected");
					$(".pm-select option:eq(0)").attr("selected", "selected");

					if($(this).hasClass("new-project") && $(".overlay-bg.project-manager .overlay-inner").hasClass("open")) {
						return false;	
					}

					$(".overlay-bg.project-manager .overlay-inner").toggleClass("open");
				});
				
				// EXPORT DATA BUTTON
				$(document).on("click", ".button.generate-csv", function() {
					var projectArray = options.data;
					
					// SORT PROJECTS ARRAY
					projectArray.sort(function(a, b) {
						// SORT BY RESOURCE
						if(a.resource < b.resource) {
							return -1;
						}
						if(a.resource > b.resource) {
							return 1;
						}

						// THEN SORT BY START DATE
						var aDate = new Date(a.deliveryDate);
						var bDate = new Date(b.deliveryDate);
						if(aDate > bDate) {
							return -1;
						}
						if(aDate < bDate) {
							return 1;
						}
						return 0;
					});
					
					var resource = "";
					var csvFile = 'Account,Project Type,Developer,Designer,Start Date,End Date,QA Date,Delivery Date,SalesForce Urls,Trello Card\n';
					
					// LOOP THROUGH THE RECORDS
					for(var r in projectArray) {
						// ONLY GRAB THE RECORD IF IT DELIVERED BEFORE TODAY
						if(projectArray[r].status != "Exempt") {
							// CHECK THE CURRENT RESOURCE AND ADD A DIVIDER IF NEEDED
							if(resource == "" || projectArray[r].resource != resource) {
								csvFile += '\n';
								resource = projectArray[r].resource;
							}
							
							csvFile += '"' + projectArray[r].accountName + '",' + projectArray[r].projectType + ',"' + projectArray[r].resource + '","' + projectArray[r].designer + '",' + projectArray[r].startDate + ',' + projectArray[r].endDate + ',' + projectArray[r].qaDate + ',' + projectArray[r].deliveryDate + ',"' + projectArray[r].salesforceProject + '",' + projectArray[r].trelloCard + '\n';
						}
					}
					
					var csvFileAsBlob = new Blob([csvFile], {type: "text/plain"});
                    $(this).attr("href", window.URL.createObjectURL(csvFileAsBlob));
				});

				// CLOSE EDITOR BUTTON
				$(document).on("click", ".button.pm-close-editor", function(e) {
					e.preventDefault();

					$(".pm-error").remove();
					$(".pm-input.error").removeClass("error");

					$(".pm-input.required").each(function() {
						if($(this).val() == $(this).attr("data-error")) {
							$(this).val("").removeClass("error");	
						}
					});

					$(".overlay-bg.project-manager .overlay-inner").toggleClass("open");
				});

				// CLEAR PROJECT INPUTS
				$(document).on("focus", ".pm-input.required", function() {
					if($(this).val() == $(this).attr("data-error")) {
						$(this).val("").removeClass("error");	
					}
				});

				// SAVE PROJECT BUTTON
				$(document).on("click", ".button.pm-save-project", function(e) {
					e.preventDefault();
					var hasError = false;
					$(".pm-error").remove();
					$(".pm-input.error").removeClass("error");

					// RUN SOME VALIDATION
					$(".pm-input.required").each(function() {
						if($.trim($(this).val()) == "" || $(this).val() == $(this).attr("data-error")) {
							$(this).val($(this).attr("data-error")).addClass("error");

							hasError = true;	
						}
					});
					$(".pm-select.required").each(function() {
						if(!$("option:selected", this).length || $("option:selected", this).val().indexOf("Choose") > -1) {
							$('<span class="pm-error">' + $(this).attr("data-error") + '</span>').insertAfter($(this));

							hasError = true;	
						}
					});

					// ALL IS GOOD
					if(!hasError) {
						// ADDING A RECORD
						if($("#pm-record-id").val() == "") {
							var projectType = ($("#pm-project-type option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-project-type option:selected").val();
							var resource = ($("#pm-resource option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-resource option:selected").val();
							var designer = ($("#pm-designer option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-designer option:selected").val();
							var phase = ($("#pm-phase option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-phase option:selected").val();
							var status = ($("#pm-status option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-status option:selected").val();

							var recordData = {
								accountName: $("#pm-account-name").val(),
								projectType: projectType,
								resource: resource,
								designer: designer,
								startDate: $("#pm-start-date").val(),
								endDate: $("#pm-end-date").val(),
								qaDate: $("#pm-qa-date").val(),
								deliveryDate: $("#pm-delivery-date").val(),
								phase: phase,
								status: status,
								salesforceProject: $("#pm-salesforce-project").val(),
								trelloCard: $("#pm-trello-card").val()
							};

							// INSERT THE RECORD
							var insertWithNewKey = FirebaseConfiguration.Action({
								"action": "insert",
								"firebaseApp": options.database,
								"ref": "/projects/",
								"recordData": recordData,
								"successMsg": "The record was saved!",
								"callback": function(data) {
									// INSERT THE NEW PROJECT KEY FOR IMMEDIATE UPDATE ABILITY
									$("#pm-record-id").val(data.newKey);
								}
							});
						}

						// UPDATING A RECORD
						else {
							var recordID = $("#pm-record-id").val();
							var projectType = ($("#pm-project-type option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-project-type option:selected").val();
							var resource = ($("#pm-resource option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-resource option:selected").val();
							var designer = ($("#pm-designer option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-designer option:selected").val();
							var phase = ($("#pm-phase option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-phase option:selected").val();
							var status = ($("#pm-status option:selected").val().indexOf("Choose") > -1) ? "" : $("#pm-status option:selected").val();

							var recordData = {
								accountName: $("#pm-account-name").val(),
								projectType: projectType,
								resource: resource,
								designer: designer,
								startDate: $("#pm-start-date").val(),
								endDate: $("#pm-end-date").val(),
								qaDate: $("#pm-qa-date").val(),
								deliveryDate: $("#pm-delivery-date").val(),
								phase: phase,
								status: status,
								salesforceProject: $("#pm-salesforce-project").val(),
								trelloCard: $("#pm-trello-card").val()
							};
							
							FirebaseConfiguration.Action({
								"action": "update",
								"firebaseApp": options.database,
								"ref": "/projects/",
								"recordID": recordID,
								"recordData": recordData,
								"successMsg": "The record was updated!"
							});
						}
					}
				});
				
				// SHOW PAST PROJECTS HEADER
				$(document).on("click", ".pm-record-header.past h2", function() {
					var pastProjectContainer = $(this).closest("#pm-past-records");
					var pastProjectRecordList = $(this).closest("#pm-past-records").find(".pm-records-list");
					
					if($(pastProjectContainer).hasClass("open")) {
						$(pastProjectContainer).removeClass("open")
						$(pastProjectRecordList).slideUp(400);
					} else {
						$(pastProjectContainer).addClass("open")
						$(pastProjectRecordList).slideDown(400);
					}
				});
				
				// QUICK SEARCH
				$(document).on("keyup", "#pm-quick-search", function() {
					var currentResultsFound = false;
					var searchVal = $.trim($(this).val()).toLowerCase();
					$("#pm-current-records .no-results, #pm-past-records .no-results").remove();

					$("#pm-current-records .pm-records-list .pm-record").each(function() {
						var recordInfo = $("button.edit-project", this).attr("data-account-name").toLowerCase();

						if(searchVal != "") {
							if(recordInfo.indexOf(searchVal) === -1) {
								$(this).hide();
							} else {
								$(this).show();

								currentResultsFound = true;
							}
						} else {
							$(this).show();
						}
					});

					if(!currentResultsFound && searchVal != "") {
						$("#pm-current-records .pm-records-list").prepend('<div class="pm-record clear no-results">No results found.</div>');
					}
					
					var pastResultsFound = false;
					$("#pm-past-records .pm-records-list .pm-record").each(function() {
						var recordInfo = $("button.edit-project", this).attr("data-account-name").toLowerCase();

						if(searchVal != "") {
							if(recordInfo.indexOf(searchVal) === -1) {
								$(this).hide();
							} else {
								$(this).show();

								pastResultsFound = true;
							}
						} else {
							$(this).show();
						}
					});
					
					$("#pm-past-records").addClass("open")
					$("#pm-past-records .pm-records-list").slideDown(400);

					if(!pastResultsFound && searchVal != "") {
						$("#pm-past-records .pm-records-list").prepend('<div class="pm-record clear no-results">No results found.</div>');
					}
				});
				
				this.projectManagerEventsNotBound = false;
			}
		},
		
		"SettingsManagerBuildView": function(options) {
			var settingsManager = "";
			var snapshot = options.snapshot;
			var developers = snapshot.child("/developers").val();
			var designers = snapshot.child("/designers").val();
			var projectTypes = snapshot.child("/projectTypes").val();
			var phases = snapshot.child("/phases").val();
			var statuses = snapshot.child("/status").val();
			var queueTime = snapshot.child("/queueTime").val();
			var holidayList = snapshot.child("/holidays").val();
			var holidays = "";

			for(var h in holidayList) {
				holidays += '<div class="sm-holiday clear">'
				holidays += 	'<p class="left"><input type="text" class="sm-holiday-name required" value="' + h + '" placeholder="Holiday Name" /> : <input type="text" class="sm-date sm-holiday-start-date required" value="' + holidayList[h].startDate + '" placeholder="Holiday Start Date" /> - <input type="text" class="sm-date sm-holiday-end-date required" value="' + holidayList[h].endDate + '" placeholder="Holiday End Date" /></p>';
				holidays += 	'<button class="button delete right sm-holiday-delete">Delete</button>';
				holidays += '</div>';
			}

			// PUT TOGETHER THE UI
			settingsManager += '<div class="settings-editor clear">';
			settingsManager += '	<div class="pm-control"><h4>Developers <span>(comma deliminated)</span></h4><input type="text" id="sm-developers" class="sm-input required" value="' + developers + '" /></div>';
			settingsManager += '	<div class="pm-control"><h4>Designers <span>(comma deliminated)</span></h4><input type="text" id="sm-designers" class="sm-input required" value="' + designers + '" /></div>';
			settingsManager += '	<div class="pm-control"><h4>Project Types <span>(comma deliminated)</span></h4><input type="text" id="sm-project-types" class="sm-input required" value="' + projectTypes + '" /></div>';
			settingsManager += '	<div class="pm-control"><h4>Phases <span>(comma deliminated)</span></h4><input type="text" id="sm-phases" class="sm-input required" value="' + phases + '" /></div>';
			settingsManager += '	<div class="pm-control"><h4>Statuses <span>(comma deliminated)</span></h4><input type="text" id="sm-statuses" class="sm-input required" value="' + statuses + '" /></div>';
			settingsManager += '	<div class="pm-control"><h4>Current Development Timeline</h4><input type="text" id="sm-queue-time" class="sm-input required" value="' + queueTime + '" /></div>';
			settingsManager += '	<div class="pm-control sm-holidays"><h4>Holidays</h4>' + holidays + '<button id="sm-add-holiday" class="button left">Add Holiday</button></div>';
			settingsManager += '	<button class="button right sm-save-settings">Save</button>';
			settingsManager += '	</div>';

			// ADD EVERYTHING TO THE DOM
			$(".overlay-outer.settings-manager .overlay-inner").html(settingsManager);

			// BIND THE DATEPICKER
			$(".sm-date").datepicker();
		},
			
		"SettingsManagerUiEvents": function(options) {
			if(this.settingsManagerEventsNotBound) {
				// SETTINGS MANAGER BUTTON
				$(document).on("click", ".button.settings", function(e) {
					e.preventDefault();

					$(".overlay-bg.settings-manager").fadeIn();
				});

				// ADD HOLIDAY
				$(document).on("click", "#sm-add-holiday", function(e) {
					e.preventDefault();

					$('<div class="sm-holiday clear"><p class="left"><input type="text" class="sm-holiday-name required" placeholder="Holiday Name" /> : <input type="text" class="sm-date sm-holiday-start-date required" placeholder="Holiday Start Date" /> - <input type="text" class="sm-date sm-holiday-end-date required" placeholder="Holiday End Date" /></p><button class="button delete right sm-holiday-delete">Delete</button></div>').insertBefore($(this));

					// REBIND THE DATEPICKER
					$(".sm-date").datepicker();
				});

				// DELETE HOLIDAY
				$(document).on("click", ".sm-holiday-delete", function(e) {
					e.preventDefault();
					var _this = this;
					
					// CONFIRM BEFORE WE DELETE
					ProjectManager.LightBox({
						"message": '<h2>Hold on a tick!</h2><div class="light-box-message">Are you sure you want to delete this holiday?</div>',
						"confirm": function() {
							$(_this).parent().remove();

							// UPDATE ALL SETTINGS
							var holidays = {};
							$(".sm-holiday").each(function() {
								var holidayName = $.trim($(".sm-holiday-name", this).val());
								var holidayStartDate = $.trim($(".sm-holiday-start-date", this).val());
								var holidayEndDate = $.trim($(".sm-holiday-end-date", this).val());

								holidays[holidayName] = {
									startDate: holidayStartDate,
									endDate: holidayEndDate
								}
							});
							
							var settingsData = {
								developers: $.trim($("#sm-developers").val()),
								designers: $.trim($("#sm-designers").val()),
								holidays: holidays,
								phases: $.trim($("#sm-phases").val()),
								projectTypes: $.trim($("#sm-project-types").val()),
								status: $.trim($("#sm-statuses").val()),
								queueTime: $.trim($("#sm-queue-time").val())
							};

							FirebaseConfiguration.Action({
								"action": "update",
								"firebaseApp": options.database,
								"ref": "/",
								"recordID": "",
								"recordData": settingsData,
								"successMsg": "The settings were updated!"
							});
						},
						"cancel": function() {},
						"showCancel": true
					});
				});

				// SAVE SETTINGS BUTTON
				$(document).on("click", ".button.sm-save-settings", function(e) {
					e.preventDefault();

					// UPDATE ALL SETTINGS
					var holidays = {};
					$(".sm-holiday").each(function() {
						var holidayName = $.trim($(".sm-holiday-name", this).val());
						var holidayStartDate = $.trim($(".sm-holiday-start-date", this).val());
						var holidayEndDate = $.trim($(".sm-holiday-end-date", this).val());

						holidays[holidayName] = {
							startDate: holidayStartDate,
							endDate: holidayEndDate
						}
					});

					var settingsData = {
						developers: $.trim($("#sm-developers").val()),
						designers: $.trim($("#sm-designers").val()),
						holidays: holidays,
						phases: $.trim($("#sm-phases").val()),
						projectTypes: $.trim($("#sm-project-types").val()),
						status: $.trim($("#sm-statuses").val()),
						queueTime: $.trim($("#sm-queue-time").val())
					};
					
					FirebaseConfiguration.Action({
						"action": "update",
						"firebaseApp": options.database,
						"ref": "/",
						"recordID": "",
						"recordData": settingsData,
						"successMsg": "The settings were updated!"
					});
				});
				
				this.settingsManagerEventsNotBound = false;
			}
		}/*,
		// KEPT THIS IN CASE I WANT TO USE IT AGAIN LATER
		"HouseCleaning": function(options) {
			if(!ProjectManager.ReadCookie("houseCleaning") || ProjectManager.ReadCookie("houseCleaning") === null) {
				// AUTO DELETE PTO RECORDS
				var today = new Date();
				var projectNum = 0;
				var projects = options.projects;

				// CHECK FOR OLD PROJECTS
				for(var p in projects) {
					var accountName = projects[p].accountName.toLowerCase();
					var projectDate = new Date(projects[p].deliveryDate);
					if((accountName == "pto" || accountName == "flex time") && projectDate.getTime() < today.getTime()) {
						var recordID = p;

						FirebaseConfiguration.Action({
							"action": "delete",
							"firebaseApp": options.database,
							"ref": "/projects/",
							"recordID": recordID,
							"recordData": "",
							"successMsg": ""
						});

						projectNum++;
					}
				}

				// GIVE SOME FEEDBACK IF PROJECTS WERE DELETED
				if(projectNum > 0) {
					var notifyMsg = (projectNum == 1) ? "Did some house cleaning. 1 PTO record has been deleted." : "Did some house cleaning. " + projectNum + " PTO records have been deleted.";
					ProjectManager.Notify(notifyMsg);
				}

				// SET OR RESET THE COOKIE
				ProjectManager.CreateCookie("houseCleaning", true, 7);
			}
		}*/
		
	};
	
	// KICK EVERYTHING OFF
	ProjectManager.Init();
	
});